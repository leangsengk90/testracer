import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";

export default class Home extends Component {
    constructor(){
        super();
        this.state = {
            char: [],
            getTyping: [],
            color: "",
        }
    }


    onChange(e){
        // console.log(e.target.value);
        let getTyping = e.target.value;
        this.setState({
             getTyping: [],
        })
        getTyping.split("").forEach(i=>{
            this.state.getTyping.push(i.charCodeAt(0));
        })
        //console.log(this.state.getTyping);
        this.state.getTyping.map((item,index)=>{
            console.log(item+":"+this.state.char[index])
            if(item === this.state.char[index]){
                this.setState({
                    color: "lightgreen",
                })
            }else{
                this.setState({
                    color: "red",
                })
            }
        })
    }

  render() {
    let words = "ប្រទេសកម្ពុជាមានផ្ទៃដី ១៨១ ០៣៥ គីឡូម៉ែត្រការ៉េ ។";
    
    words.split("").forEach((char) => {
      //console.log(char);
        this.state.char.push(char.charCodeAt(0));
    });

    return (
      <div className="container">
        <div className="row text-left">
          <div className="col-md">
            <Form>
              <h5 style={{backgroundColor: `${this.state.color}`}}>{words}</h5>
              <Form.Group controlId="formBasicEmail">
                {/* <Form.Label>Text</Form.Label> */}
                <Form.Control 
                type="email" 
                placeholder="Typing here..." 
                onInput={this.onChange.bind(this)} 
                />
                <Form.Text className="text-muted">
                  {/* We'll never share your email with anyone else. */}
                </Form.Text>
              </Form.Group>

              {/* <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group> */}
              {/* <Form.Group controlId="formBasicCheckbox">
                <Form.Check type="checkbox" label="Check me out" />
              </Form.Group> */}
              {/* <Button variant="primary" type="submit">
                Submit
              </Button> */}
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
