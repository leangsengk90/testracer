import React from "react";
import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from "./Home";
import Main from "./Main";

function App() {
  return (
    <div className="App">
      <Main />
      <Home />
    </div>
  );
}

export default App;
